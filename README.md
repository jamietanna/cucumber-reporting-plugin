# A reporter plug-in for pretty Cucumber reports

[![pipeline status](https://gitlab.com/monochromata-de/cucumber-reporting-plugin/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/cucumber-reporting-plugin/commits/master)

Wraps [damianszczepanik/cucumber-reporting](https://github.com/damianszczepanik/cucumber-reporting)
in a Cucumber plug-in so it can be used directly when running Cucumber.

It delegates to the CucumberJsonReporter on-the-fly and finally passes the JSON to
[damianszczepanik/cucumber-reporting](https://github.com/damianszczepanik/cucumber-reporting). 

## Usage

Also consider using

* [Cucumber Reports](https://reports.cucumber.io/),
* [Cucumber Studio](https://cucumber.io/tools/cucumberstudio/),
* [damianszczepanik/maven-cucumber-reporting](https://github.com/damianszczepanik/maven-cucumber-reporting),
* [damianszczepanik/cucumber-sandwich](https://github.com/damianszczepanik/cucumber-sandwich),
* [damianszczepanik/cucumber-reporting-jenkins](https://github.com/damianszczepanik/cucumber-reporting-jenkins), or
* [damianszczepanik/cucumber-reporting](https://github.com/damianszczepanik/cucumber-reporting)

The plug-in is provided through the Central repository:

```
<dependency>
  <groupId>me.jvt.cucumber</groupId>
  <artifactId>reporting-plugin</artifactId>
  <version>5.0.0</version>
</dependency>

```

### Configuring (optional)

The reporter plug-in reads configuration from a properties file configured
via the system property `cucumber.reporting.config.file` that defaults
to `./cucumber-reporting.properties`.

The properties file should be in UTF-8 format. The following keys are
accepted:

* `projectName` (String)
* `buildNumber` (String)
* `sortingMethod` (String, [defaults to NATURAL in cucumber-reports](https://github.com/damianszczepanik/cucumber-reporting/blob/master/src/main/java/net/masterthought/cucumber/Configuration.java#L33)) (Only acceptable string values are one of [`NATURAL`|`ALPHABETICAL`])
* `tagsToExcludeFromChart.<XXX>` (String, `<XXX>` should be replaced by a number starting from 1 to n, so to account for multiple tag regex patterns to be excluded from the tag chart.)
* `trendsStatsFile` (String, file path string to a json file containing trend stats)
* `presentationMode.<XXX>` (boolean, `<XXX>` should be replaced by one of these only acceptable string values [`RUN_WITH_JENKINS`|`EXPAND_ALL_STEPS`], repetition is allowed and the value of the property must be true to activate it.)
* `reducingMethod.<XXX>` (boolean, `<XXX>` should be replaced by one of these only acceptable string values representing [reducing methods](https://github.com/damianszczepanik/cucumber-reporting/blob/master/src/main/java/net/masterthought/cucumber/reducers/ReducingMethod.java) [`MERGE_FEATURES_BY_ID`|`MERGE_FEATURES_WITH_RETEST`|`SKIP_EMPTY_JSON_FILES`|`HIDE_EMPTY_HOOKS`], repetition is allowed and the value of the property must be true to activate it.)
* `classifications.<XXX>` (String, `<XXX>` should be replaced by a desired sub-key, multiple `classifications.<XXX>` keys are possible with different sub-keys)

### Cucumber CLI

Register the plug-in with the cucumber run-time using any of `-p`,
`-plugin` or `--add-plugin`:

```
java cucumber.api.cli.Main --add-plugin me.jvt.cucumber.report.PrettyReports:target/cucumber
```

### Cucumber JUnit Runner

> **NOTE:** The plug-in will only run with the current cucumber libraries from the [io.cucumber group](https://search.maven.org/search?q=g:io.cucumber). If you still use the outdated cucumber libraries from the old [info.cukes group](https://search.maven.org/search?q=g:info.cukes), you will get a [CucumberException: Unrecognized plugin: me.jvt.cucumber.report.PrettyReports](https://gitlab.com/monochromata-de/cucumber-reporting-plugin/issues/19).

```
@CucumberOptions(plugin = { "me.jvt.cucumber.report.PrettyReports:target/cucumber" })
```
