package me.jvt.cucumber.report.config;

import static java.lang.Boolean.parseBoolean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.reducers.ReducingMethod;
import net.masterthought.cucumber.sorting.SortingMethod;
import org.apache.commons.lang3.StringUtils;

public class ConfigurationFactory {

  private static final String DEFAULT_FILENAME = "cucumber-reporting.properties";
  private static final String TAGS_TO_EXCLUDE_FROM_CHART_PATTERN =
      "^tagsToExcludeFromChart\\.\\d+$";
  private static final String PRESENTATION_MODE_PREFIX = "presentationMode.";
  private static final String REDUCING_METHOD_PREFIX = "reducingMethod.";
  private static final String CLASSIFICATIONS_PREFIX = "classifications.";
  public static final String CONFIG_FILE_PROPERTY = "cucumber.reporting.config.file";

  public static Configuration getConfiguration(File outputDir) {
    Properties properties = loadProperties();
    String projectName =
        properties.getProperty(
            "projectName", "No Name (add projectName to cucumber-reporting.properties)");
    Configuration configuration = new Configuration(outputDir, projectName);
    configureBuildNumber(configuration, properties);
    configureSortingMethod(configuration, properties);
    configureTagsToExcludeFromChart(configuration, properties);
    configureTrendsStatsFile(configuration, properties);
    configureRepeatableConfigurationKeys(configuration, properties);
    return configuration;
  }

  protected static void configureBuildNumber(Configuration configuration, Properties properties) {
    configuration.setBuildNumber(properties.getProperty("buildNumber"));
  }

  protected static void configureSortingMethod(Configuration configuration, Properties properties) {
    String sortingMethod = properties.getProperty("sortingMethod");
    if (StringUtils.isNotEmpty(sortingMethod)) {
      configuration.setSortingMethod(Enum.valueOf(SortingMethod.class, sortingMethod));
    }
  }

  protected static void configureTagsToExcludeFromChart(
      Configuration configuration, Properties properties) {
    String[] tagsToExclude = getTagsToExcludeFromChart(properties);
    configuration.setTagsToExcludeFromChart(tagsToExclude);
  }

  protected static String[] getTagsToExcludeFromChart(Properties properties) {
    return properties.entrySet().stream()
        .filter(entry -> ((String) entry.getKey()).matches(TAGS_TO_EXCLUDE_FROM_CHART_PATTERN))
        .map(Entry::getValue)
        .toArray(String[]::new);
  }

  protected static void configureTrendsStatsFile(
      Configuration configuration, Properties properties) {
    String trendsStatsFile = properties.getProperty("trendsStatsFile");
    if (StringUtils.isNotEmpty(trendsStatsFile)) {
      configuration.setTrendsStatsFile(new File(trendsStatsFile));
    }
  }

  protected static void configureRepeatableConfigurationKeys(
      Configuration configuration, Properties properties) {
    Enumeration<Object> keys = properties.keys();
    while (keys.hasMoreElements()) {
      String qualifiedKey = (String) keys.nextElement();
      if (qualifiedKey.startsWith(PRESENTATION_MODE_PREFIX)) {
        configurePresentationMode(qualifiedKey, configuration, properties);
      } else if (qualifiedKey.startsWith(REDUCING_METHOD_PREFIX)) {
        configureReducingMethod(qualifiedKey, configuration, properties);
      } else if (qualifiedKey.startsWith(CLASSIFICATIONS_PREFIX)) {
        configureClassifications(qualifiedKey, configuration, properties);
      }
    }
  }

  protected static void configurePresentationMode(
      String qualifiedKey, Configuration configuration, Properties properties) {
    if (parseBoolean(properties.getProperty(qualifiedKey))) {
      String presentationModeName = qualifiedKey.substring(PRESENTATION_MODE_PREFIX.length());
      PresentationMode presentationMode =
          Enum.valueOf(PresentationMode.class, presentationModeName);
      configuration.addPresentationModes(presentationMode);
    }
  }

  protected static void configureReducingMethod(
      String qualifiedKey, Configuration configuration, Properties properties) {
    if (parseBoolean(properties.getProperty(qualifiedKey))) {
      String reducingMethodName = qualifiedKey.substring(REDUCING_METHOD_PREFIX.length());
      ReducingMethod reducingMethod = Enum.valueOf(ReducingMethod.class, reducingMethodName);
      configuration.addReducingMethod(reducingMethod);
    }
  }

  protected static void configureClassifications(
      String qualifiedKey, Configuration configuration, Properties properties) {
    String key = qualifiedKey.substring(CLASSIFICATIONS_PREFIX.length());
    configuration.addClassifications(key, properties.getProperty(qualifiedKey));
  }

  protected static Properties loadProperties() {
    Properties properties = new Properties();
    InputStream stream = getPropertiesStream();
    if (stream != null) {
      try (InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8)) {
        properties.load(reader);
      } catch (IOException e) {
        throw new UncheckedIOException(e);
      }
    }
    return properties;
  }

  protected static InputStream getPropertiesStream() {
    String filename = getPropertiesFilename();
    Optional<File> propertiesFile =
        Optional.of(Paths.get(filename).toFile())
            .filter(File::exists)
            .filter(File::isFile)
            .filter(File::canRead);
    if (!propertiesFile.isPresent() && !filename.equals(DEFAULT_FILENAME)) {
      throw new UncheckedIOException(
          new FileNotFoundException(
              "Cucumber reporting properties file " + filename + " was not found"));
    } else if (propertiesFile.isPresent()) {
      try {
        return new FileInputStream(propertiesFile.get());
      } catch (FileNotFoundException e) {
        throw new UncheckedIOException(e);
      }
    }
    return null;
  }

  protected static String getPropertiesFilename() {
    return System.getProperty(CONFIG_FILE_PROPERTY, DEFAULT_FILENAME);
  }

  private ConfigurationFactory() {
    // hide implicit constructor of utility class
  }
}
