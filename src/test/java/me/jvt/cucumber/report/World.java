package me.jvt.cucumber.report;

import java.nio.file.Path;

public class World {

  private Path featureFile;
  private Path testOutputDir;
  private Path propertiesFile;
  private Path trendsStatsFile;

  public Path getFeatureFile() {
    return featureFile;
  }

  public void setFeatureFile(Path featureFile) {
    this.featureFile = featureFile;
  }

  public Path getTestOutputDir() {
    return testOutputDir;
  }

  public void setTestOutputDir(Path testOutputDir) {
    this.testOutputDir = testOutputDir;
  }

  public Path getPropertiesFile() {
    return propertiesFile;
  }

  public void setPropertiesFile(Path propertiesFile) {
    this.propertiesFile = propertiesFile;
  }

  public Path getTrendsStatsFile() {
    return trendsStatsFile;
  }

  public void setTrendsStatsFile(Path trendsStatsFile) {
    this.trendsStatsFile = trendsStatsFile;
  }
}
