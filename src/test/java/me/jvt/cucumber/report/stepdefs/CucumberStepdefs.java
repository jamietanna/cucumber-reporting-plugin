package me.jvt.cucumber.report.stepdefs;

import static java.util.Optional.ofNullable;
import static org.assertj.core.api.Assertions.assertThat;

import io.cucumber.core.cli.Main;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Path;
import me.jvt.cucumber.report.World;

public class CucumberStepdefs {

  private final World world;
  private String[] args;
  private byte exitCode;
  private String output;
  private Exception exception;

  public CucumberStepdefs(World world) {
    this.world = world;
  }

  @Given("Cucumber options {string}")
  public void cucumberOptions(String args) {
    this.args =
        args.replaceAll("\\$testOutputDir", toString(world.getTestOutputDir()))
            .replaceAll("\\$propertiesFile", toString(world.getPropertiesFile()))
            .replaceAll("\\$featureFile", toString(world.getFeatureFile()))
            .split(" ");
  }

  @When("Cucumber is run")
  public void cucumberIsRun() {
    ByteArrayOutputStream systemOutBaos = new ByteArrayOutputStream();
    ByteArrayOutputStream systemErrBaos = new ByteArrayOutputStream();
    PrintStream oldSystemOut = System.out;
    PrintStream oldSystemErr = System.err;
    try {
      System.setOut(new PrintStream(systemOutBaos));
      System.setErr(new PrintStream(systemErrBaos));
      exitCode = Main.run(args, Thread.currentThread().getContextClassLoader());
    } catch (Exception e) {
      exception = e;
    } finally {
      System.setOut(oldSystemOut);
      System.setErr(oldSystemErr);
      output = systemOutBaos.toString();
    }
  }

  @Then("Cucumber finishes successfully and output matches")
  public void cucumberFinishesSuccessfullyAndOutputMatches(String expectedOutput) {
    assertThat(output).matches(expectedOutput);
    assertThat(exitCode).isEqualTo((byte) 0);
  }

  @Then("Cucumber finishes with exit code {int}")
  public void cucumberFinishesWithExitCode(Integer expectedExitCode) {
    assertThat(exitCode).isEqualTo(expectedExitCode.byteValue());
  }

  @Then("Cucumber finishes with exit code {int} and output matches")
  public void cucumberFinishesWithExitCodeAndOutputMatches(
      Integer expectedExitCode, String expectedOutput) {
    assertThat(output).matches(expectedOutput);
    cucumberFinishesWithExitCode(expectedExitCode);
  }

  @Then("a {string} is thrown with message {string}")
  public void anExceptionIsThrownWithMessage(String exceptionTypeName, String messageTemplate)
      throws ClassNotFoundException {
    String message =
        messageTemplate.replaceAll("\\$propertiesFile", world.getPropertiesFile().toString());
    assertThat(exception).isInstanceOf(Class.forName(exceptionTypeName)).hasMessage(message);
  }

  protected String toString(Path path) {
    return ofNullable(path).map(Path::toAbsolutePath).map(Path::toString).orElse("");
  }
}
