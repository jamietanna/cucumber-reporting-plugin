package me.jvt.cucumber.report.stepdefs;

import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;

import io.cucumber.java.en.Then;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;
import me.jvt.cucumber.report.World;

public class ReportStepdefs {

  private final World world;

  public ReportStepdefs(World world) {
    this.world = world;
  }

  @Then("the test output dir testOutputDir contains an html report for project {string}")
  public void htmlReportForProject(String projectName) throws IOException {
    String overviewContents = getFeaturesOverviewContents(world);
    assertThat(overviewContents).contains("<td>" + projectName + "</td>");
  }

  @Then("the test output dir testOutputDir contains an html report for build number {string}")
  public void htmlReportForBuildNumber(String buildNumber) throws IOException {
    String overviewContents = getFeaturesOverviewContents(world);
    assertThat(overviewContents).contains("<td>" + buildNumber + "</td>");
  }

  @Then(
      "the test output dir testOutputDir contains an html report for classification {string} {string}")
  public void htmlReportForClassification(String key, String value) throws IOException {
    String overviewContents = getFeaturesOverviewContents(world);
    assertThat(overviewContents).contains("<th>" + key + "</th>");
    assertThat(overviewContents).contains("<td>" + value + "</td>");
  }

  @Then("the tags overview in test output dir testOutputDir includes tag {string}")
  public void tagsOverviewIncludesTag(String tag) throws IOException {
    String tagsOverviewContents = getTagsOverviewContents(world);
    assertThat(tagsOverviewContents)
        .containsPattern(String.format("<td class=\"tagname\"><a href=\".*\">%s</a></td>", tag));
  }

  @Then("the tags overview in test output dir testOutputDir excludes tag {string}")
  public void tagsOverviewExcludesTag(String tag) throws IOException {
    String tagsOverviewContents = getTagsOverviewContents(world);
    Pattern pattern =
        Pattern.compile(String.format(".*<td class=\"tagname\"><a href=\".*\">%s</a></td>.*", tag));
    assertThat(tagsOverviewContents).doesNotMatch(pattern);
  }

  protected String getFeaturesOverviewContents(World world) throws IOException {
    Path featuresOverview = getFeaturesOverview(world);
    assertThat(featuresOverview).exists();
    return getFileOutputFromCucumberReporter(featuresOverview);
  }

  protected String getTagsOverviewContents(World world) throws IOException {
    Path tagsOverview = getTagsOverview(world);
    assertThat(tagsOverview).exists();
    return getFileOutputFromCucumberReporter(tagsOverview);
  }

  protected Path getFeaturesOverview(World world) {
    return world
        .getTestOutputDir()
        .resolve("cucumber-html-reports")
        .resolve("overview-features.html");
  }

  protected Path getTagsOverview(World world) {
    return world.getTestOutputDir().resolve("cucumber-html-reports").resolve("overview-tags.html");
  }

  protected String getFileOutputFromCucumberReporter(Path path) throws IOException {
    return Files.lines(path, StandardCharsets.UTF_8).collect(joining(" "));
  }
}
