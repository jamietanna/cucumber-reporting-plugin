package me.jvt.cucumber.report.stepdefs;

import static java.lang.System.lineSeparator;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.lines;
import static java.nio.file.Files.write;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.joining;
import static me.jvt.cucumber.report.config.ConfigurationFactory.CONFIG_FILE_PROPERTY;
import static org.assertj.core.api.Assertions.assertThat;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import java.io.IOException;
import java.nio.file.Paths;
import me.jvt.cucumber.report.World;

public class FileStepdefs {

  private boolean defaultPropertiesFileCreated = false;
  private boolean customPropertiesFileCreated = false;
  private String originalTrendsStats;
  private final World world;

  public FileStepdefs(World world) {
    this.world = world;
  }

  @Given("test output directory $testOutputDir")
  public void testOutputDirectory() throws IOException {
    world.setTestOutputDir(createTempDirectory("testOutputDir"));
    world.getTestOutputDir().toFile().deleteOnExit();
  }

  @Given("properties file in default location containing")
  public void propertiesFileInDefaultLocation(String contentsWithPlaceholders) throws IOException {
    world.setPropertiesFile(Paths.get("cucumber-reporting.properties"));
    writePropertiesFile(world, contentsWithPlaceholders);
    defaultPropertiesFileCreated = true;
  }

  @Given("properties file $propertiesFile containing")
  public void propertiesFile(String contentsWithPlaceholders) throws IOException {
    world.setPropertiesFile(createTempFile("propertiesFile", ".properties"));
    writePropertiesFile(world, contentsWithPlaceholders);
    System.setProperty(CONFIG_FILE_PROPERTY, world.getPropertiesFile().toString());
    customPropertiesFileCreated = true;
  }

  @Given("trends stats file $trendsStatsFile containing")
  public void trendsStatsFile(String contents) throws IOException {
    originalTrendsStats = contents;
    world.setTrendsStatsFile(createTempFile("trendsStatsFile", ".json"));
    write(world.getTrendsStatsFile(), singletonList(contents));
  }

  @Given("a non-existent properties file $propertiesFile")
  public void nonExistentPropertiesFile() throws IOException {
    world.setPropertiesFile(createTempFile("propertiesFile", ".properties"));
    world.getPropertiesFile().toFile().delete();
    System.setProperty(CONFIG_FILE_PROPERTY, world.getPropertiesFile().toString());
  }

  @Then("the trends stats file has been updated")
  public void trendsStatsFileHasBeenUpdated() throws IOException {
    String updatedTrendsStats = lines(world.getTrendsStatsFile()).collect(joining(lineSeparator()));
    assertThat(updatedTrendsStats).isNotEqualToIgnoringWhitespace(originalTrendsStats);
  }

  @After
  public void after(Scenario scenario) {
    if (defaultPropertiesFileCreated) {
      world.getPropertiesFile().toFile().delete();
      defaultPropertiesFileCreated = false;
    }
    if (customPropertiesFileCreated) {
      System.setProperty(CONFIG_FILE_PROPERTY, "");
      world.getPropertiesFile().toFile().delete();
    }
  }

  protected void writePropertiesFile(World world, String contentsWithPlaceholders)
      throws IOException {
    String contents = replaceTrendsStatsFilePlaceholder(contentsWithPlaceholders, world);
    write(world.getPropertiesFile(), singletonList(contents));
  }

  protected String replaceTrendsStatsFilePlaceholder(String contents, World world) {
    if (world.getTrendsStatsFile() != null) {
      return contents.replaceAll("\\$trendsStatsFile", world.getTrendsStatsFile().toString());
    }
    return contents;
  }
}
