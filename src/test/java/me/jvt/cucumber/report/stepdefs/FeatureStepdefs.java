package me.jvt.cucumber.report.stepdefs;

import static java.util.Collections.singletonList;

import io.cucumber.java.en.Given;
import java.io.IOException;
import java.nio.file.Files;
import me.jvt.cucumber.report.World;

public class FeatureStepdefs {

  private final World world;

  public FeatureStepdefs(World world) {
    this.world = world;
  }

  @Given("Feature $featureFile")
  public void featureFile(String contents) throws IOException {
    world.setFeatureFile(Files.createTempFile("Test", ".feature"));
    world.getFeatureFile().toFile().deleteOnExit();
    Files.write(world.getFeatureFile(), singletonList(contents));
  }
}
