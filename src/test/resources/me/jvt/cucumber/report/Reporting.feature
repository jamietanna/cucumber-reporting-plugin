Feature: Reporting

  Background:
    Given Feature $featureFile
      """
      Feature: Test

       @hello @world @exclude
       Scenario: Test
         When something is done
         Then something happens
      """

  Scenario: Running without features and plug-in arguments yields no error
    Given Cucumber options "--plugin me.jvt.cucumber.report.PrettyReports --glue non.existent.pkg doesNotExist.feature"
    When Cucumber is run
    Then Cucumber finishes successfully and output matches
      """
      """

  Scenario: Running with feature but without plug-in arguments generates report
    Given test output directory $testOutputDir
    And Cucumber options "--plugin me.jvt.cucumber.report.PrettyReports:$testOutputDir --glue non.existent.pkg $featureFile"
    When Cucumber is run
    Then Cucumber finishes with exit code 1 and output matches
      """
      (?s).*Undefined scenarios:.*

      1 Scenarios (.*1 undefined.*)
      2 Steps (.*1 skipped.*,.*1 undefined.*)
      .*
      """
    And the test output dir testOutputDir contains an html report for project "No Name (add projectName to cucumber-reporting.properties)"

  Scenario: Running with feature and cucumber-reporting.properties in default location
    Given test output directory $testOutputDir
    And Cucumber options "--plugin me.jvt.cucumber.report.PrettyReports:$testOutputDir --glue non.existent.pkg $featureFile"
    And properties file in default location containing
      """
            # Generated file
            projectName=Test project
      """
    When Cucumber is run
    Then Cucumber finishes with exit code 1 and output matches
      """
      (?s).*Undefined scenarios:.*

      1 Scenarios (.*1 undefined.*)
      2 Steps (.*1 skipped.*,.*1 undefined.*)
      .*
      """
    And the test output dir testOutputDir contains an html report for project "Test project"

  Scenario: Running with feature and cucumber-reporting.properties in non-default location
    Given test output directory $testOutputDir
    And Cucumber options "--plugin me.jvt.cucumber.report.PrettyReports:$testOutputDir --glue non.existent.pkg $featureFile"
    And trends stats file $trendsStatsFile containing
      """
        {
          "buildNumbers" : [ "01_first", "other build", "05last", "101" ],
          "passedFeatures" : [ 9, 18, 25, 1 ],
          "failedFeatures" : [ 1, 2, 5, 1 ],
          "totalFeatures" : [ 10, 20, 30, 2 ],
          "passedScenarios" : [ 10, 20, 20, 2 ],
          "failedScenarios" : [ 10, 20, 20, 2 ],
          "totalScenarios" : [ 10, 2, 5, 4 ],
          "passedSteps" : [ 1, 3, 5, 15 ],
          "failedSteps" : [ 10, 30, 50, 1 ],
          "skippedSteps" : [ 100, 300, 500, 2 ],
          "pendingSteps" : [ 1000, 3000, 5000, 1 ],
          "undefinedSteps" : [ 10000, 30000, 50000, 3 ],
          "totalSteps" : [ 100000, 300000, 500000, 22 ],
          "durations" : [ 3206126182398, 3206126182399, 3206126182310, 99355732889 ]
        }
      """
    And properties file $propertiesFile containing
      """
            # Generated file
            projectName=Test project
            buildNumber=1234
            sortingMethod=NATURAL
            presentationMode.RUN_WITH_JENKINS=true
            presentationMode.EXPAND_ALL_STEPS=false
            tagsToExcludeFromChart.1=@exclude
            trendsStatsFile=$trendsStatsFile
            reducingMethod.MERGE_FEATURES_BY_ID=true
            reducingMethod.SKIP_EMPTY_JSON_FILES=true
            reducingMethod.HIDE_EMPTY_HOOKS=false
            classifications.Platform=Linux
            classifications.Browser=Firefox 102
            classifications.Branch=master or so
      """
    When Cucumber is run
    Then Cucumber finishes with exit code 1 and output matches
      """
      (?s).*Undefined scenarios:.*

      1 Scenarios (.*1 undefined.*)
      2 Steps (.*1 skipped.*,.*1 undefined.*)
      .*
      """
    And the test output dir testOutputDir contains an html report for project "Test project"
    And the test output dir testOutputDir contains an html report for build number "1234"
    And the tags overview in test output dir testOutputDir includes tag "@hello"
    And the tags overview in test output dir testOutputDir includes tag "@world"
    And the tags overview in test output dir testOutputDir excludes tag "@exclude"
    And the trends stats file has been updated
    And the test output dir testOutputDir contains an html report for classification "Platform" "Linux"
    And the test output dir testOutputDir contains an html report for classification "Browser" "Firefox 102"
    And the test output dir testOutputDir contains an html report for classification "Branch" "master or so"

  Scenario: Properties file in configured custom location is not found
    Given test output directory $testOutputDir
    And Cucumber options "--plugin me.jvt.cucumber.report.PrettyReports:$testOutputDir $featureFile"
    And a non-existent properties file $propertiesFile
    When Cucumber is run
    Then a "java.io.UncheckedIOException" is thrown with message "java.io.FileNotFoundException: Cucumber reporting properties file $propertiesFile was not found"
